var socket = io();
$(document).ready(function(){
	$('button').click(function(){

			var s = Snap("#svg");
			var bigCircle = s.circle(150, 150, 100);
			console.log(typeof(bigCircle));
			bigCircle.attr({
					fill: "#bada55",
					stroke: "#000",
					strokeWidth: 5
			});
			socket.emit('circle', bigCircle.toJSON());
		socket.emit('button',$('#svg').html());
	});
	console.log('done 2');
});



$(function () {
	socket.on('circle', function(data){
		var s = Snap("#svg");
		s.append(Snap.parse(data));
		console.log('done 3');
		$('#p1').text(data);
	//window.scrollTo(0, document.body.scrollHeight);
 });

 socket.on('button', function(data){
	console.log('button handler');
	$('#svg').html(data);
	//window.scrollTo(0, document.body.scrollHeight);
 });

 socket.on('welcome', function(data){
	console.log(data);
	//window.scrollTo(0, document.body.scrollHeight);
 });
});

//////////////////////////////////////////////////////////////////////////////////////

(function() {

	var socket = io();
	var canvas = document.getElementById('svg');
	console.log(canvas);
	var colors = document.getElementsByClassName('color');
	var context = Snap('#svg');

	/*var paper = Snap().attr({width:"500",height:"500"});
var line = paper.line(0,0,100,100)
		.attr({strokeWidth:5,stroke:"black",strokeLinecap:"round"});*/

	var current = {
		color: 'black'
	};
	var drawing = false;

	canvas.addEventListener('mousedown', onMouseDown, false);
	canvas.addEventListener('mouseup', onMouseUp, false);
	canvas.addEventListener('mouseout', onMouseUp, false);
	canvas.addEventListener('mousemove', throttle(onMouseMove, 10), false);

	for (var i = 0; i < colors.length; i++){
		colors[i].addEventListener('click', onColorUpdate, false);
	}

	socket.on('drawing', onDrawingEvent);

	//window.addEventListener('resize', onResize, false);
	//onResize();


	function drawLine(x0, y0, x1, y1, color, emit){
		//console.log('<line x0="'+x0+'" x1="'+x1+'" y0="'+y0+'" y1="'+y1+'" style="stroke:rgb(255,0,0);stroke-width:2;" />');
		//linepath.attr(pathString=linepath.attr(pathString)+'L'+x1+','+y1);
		//console.log(linepath.attr(pathString));
		//context.path(pathString='M'+x0+' ,'+y0+' L '+x1+' ,'+y1).attr({strokeWidth:2,stroke:color});
		//context.line(x0,y0,x1,y1).attr({strokeWidth:2,stroke:color,strokeLinecap:'round'});
		mysvg.append('<rect width="300" height="100" style="fill:rgb(0,0,255);stroke-width:3;stroke:rgb(0,0,0)" />');
		//console.log(canvas.offsetLeft);
		if (!emit) { return; }
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);


		//console.log('sending:'+ parseInt(canvas.style.width)*23);
		`socket.emit('drawing', {
			x0: x0 / w,
			y0: y0 / h,
			x1: x1 / w,
			y1: y1 / h,
			color: color
		});`
	}

	function onMouseDown(e){
		drawing = true;
		current.x = e.clientX;
		current.y = e.clientY;
		linepath = context.path(pathString='M'+current.x+','+current.y).attr({strokeWidth:2,stroke:'black'});
		//$('#svg').append('<line x1="'+10+'" y1="'+50+'" x2="'+60+'" y2="'+100+'" style="stroke:rgb(255,0,0);stroke-width:2;" />');
		
		//$('#svg').append('<path d=\"M150 90 L75 200 L225 200 L 280 90\" stroke=\"green\" stroke-width=\"3\" fill=\"none\" />');
		document.getElementById('svg').append('<path d=\"M150 90 L75 200 L225 200 L 280 90\" stroke=\"green\" stroke-width=\"3\" fill=\"none\" />');
		console.log('<path d=\"M150 90 L75 200 L225 200 L 280 90\" stroke=\"green\" stroke-width=\"3\" fill=\"none\" />');
	}

	function onMouseUp(e){
		if (!drawing) { return; }
		drawing = false;
		drawLine(current.x, current.y, e.clientX, e.clientY, current.color, true);
	}

	function onMouseMove(e){
		if (!drawing) { return; }
		drawLine(current.x, current.y, e.clientX, e.clientY, current.color, true);
		//console.log('xy'+current.x+' '+current.y + ' '+ e.clientX +' '+ e.clientY);
		current.x = e.clientX;
		current.y = e.clientY;

	}

	function onColorUpdate(e){
		current.color = e.target.className.split(' ')[1];
	}

	// limit the number of events per second
	function throttle(callback, delay) {
		var previousCall = new Date().getTime();
		return function() {
			var time = new Date().getTime();

			if ((time - previousCall) >= delay) {
				previousCall = time;
				callback.apply(null, arguments);
			}
		};
	} 

	function onDrawingEvent(data){
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);
		console.log('client: ' + w);
		drawLine(data.x0 * w, data.y0 * h, data.x1 * w, data.y1 * h, data.color);
	}

	// make the canvas fill its parent
	/* function onResize() {
		canvas.style.width = window.innerWidth;
		canvas.style.height = window.innerHeight;
	} */

})();
