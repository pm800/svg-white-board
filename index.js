const express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.npm_package_config_port || 3000;
var clients = [];
//var port = 8000;
//urls

app.use(express.static(__dirname));

app.get('/', function(req, res) {
   res.sendFile(__dirname + '/index.html');
});

app.get('/mysvg', function(req, res) {
   res.sendFile(__dirname + '/mysvg.html');
});

app.get('/svgjs', function(req, res) {
   res.sendFile(__dirname + '/svgjs.html');
});

app.get('/mainjs', function(req, res) {
   res.sendFile(__dirname + '/main.js');
});

app.get('/stylecss', function(req, res) {
   res.sendFile(__dirname + '/style.css');
});

//event handelers
//myio = io.of('/myio') ;
io.on('connection', function(socket) {
   //io.emit('drawing', {x0:30,y0:80,x1:50,y1:90});
   socket.emit('welcome', 'welcome to our site.');
   socket.on('chat message', function(msg) {
	  io.emit('chat message', msg);
   });

   socket.on('circle', function(data) {
	  socket.broadcast.emit('circle', data);
   });

   socket.on('button', function(data) {
	  socket.broadcast.emit('button', data);
   });
   /////////////////////////////////////////////////////////////////
   socket.broadcast.emit('onClientAdd', socket.id);

   clients.push(socket.id);

   socket.emit('onMyId', {
	  myId: socket.id,
	  clients: clients
   });

   socket.on('disconnect', function(data) {
	  index = clients.indexOf(socket.id);
	  if (index > -1) {
		 clients.splice(index, 1);
	  }
	  socket.broadcast.emit('onClientRemove', socket.id);
   });
   ///////////////////////////////////////////////////////////////////
   socket.on('onPenMove', function(data) {
	  ////console.log('index.js x0:',data.x0);
	  ////console.log()
	  socket.broadcast.emit('onPenMove', data);
   });

   socket.on('onRectMove', function(data) {
	  socket.broadcast.emit('onRectMove', data);
   });

   socket.on('onInitRect', function(data) {
	  socket.broadcast.emit('onInitRect', data);
   });

   socket.on('onFinishRect', function(data) {
	  socket.broadcast.emit('onFinishRect', data);
   });

   socket.on('onInitPen', function(data) {
	  socket.broadcast.emit('onInitPen', data);
   });

   socket.on('onFinishPen', function(data) {
	  socket.broadcast.emit('onFinishPen', data);
   });

   socket.on('onLineInit', function(data) {
	  socket.broadcast.emit('onLineInit', data);
   });

   socket.on('onLineMove', function(data) {
	  socket.broadcast.emit('onLineMove', data);
   });

   socket.on('onInitPolygon', function(data) {
	  socket.broadcast.emit('onInitPolygon', data);
   });

   socket.on('onPropPolygon', function(data) {
	  socket.broadcast.emit('onPropPolygon', data);
   });

   socket.on('onFinishPolygon', function(data) {
	  socket.broadcast.emit('onFinishPolygon', data);
   });

   socket.on('onPolygonMove', function(data) {
	  socket.broadcast.emit('onPolygonMove', data);
   });
});

//sever listening
http.listen(port, function() {
   ////console.log('listening on *:' + port);
});

//socket.emit()   will send
