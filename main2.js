(function() {
	/*	var ir = 0,  iline=0, ipolygon=0;
		var ir_2 = 0,  iline_2=0, ipolygon_2=0;*/
	/*	var points = '';
		var points_2 = '';*/
	var info = {};
	////console.log('info json: '+JSON.stringify(info));
	////console.log('mousedown json: '+JSON.stringify(ilist));
	var myId = '';



	var socket = io();
	var canvas = document.getElementById('mysvg');

	var canvasWidth = parseInt(canvas.style.width);
	var canvasHeight = parseInt(canvas.style.height);
	////console.log(canvas);
	var colors = document.getElementsByClassName('color');
	//var context = Snap('#svg');
	var context = SVG('mysvg'); // using svg.js


	var cntrlIsPressed = false;

	$(document).ready(function() {
		$(document).keydown(function(event) {
			if (event.which == "17")
				cntrlIsPressed = true;
		});
		$(document).keyup(function(event) {
			if (event.which == "17")
				cntrlIsPressed = false;
		});
		$('#pen').click(function() {
			current.mode = 'pen';
		});
		$('#rect').click(function() {
			current.mode = 'rect';
		});
		$('#line').click(function() {
			current.mode = 'line';
		});
		$('#polygon').click(function() {
			current.mode = 'polygon';
		});
		$('#increaseWidth').click(function() {
			current.strokeWidth = parseInt(current.strokeWidth) + 1 + '';
		});
		$('#decreaseWidth').click(function() {
			if (parseInt(current.strokeWidth) >= 2) {
				current.strokeWidth = parseInt(current.strokeWidth) - 1 + '';
			}
		});
/*		$(".colors").hover(function(){
		    
		    }, function(){
		    $(this).css("background-color", "pink");
		});*/
	});

	var current = {
		color: 'black',
		mode: 'pen',
		strokeWidth: '3px',
		fill: 'none'
	};
	var initial = {
		x: '0'
	};
	var drawing = false;

	canvas.addEventListener('mousedown', onMouseDown, false);
	canvas.addEventListener('mouseup', onMouseUp, false);

	//canvas.addEventListener('mouseout', onMouseUp, false);
	//$('svg').mouseleave(onMouseUp);
	$('svg').on('mousemove', throttle(onMouseMove, 10));
	/*	$('svg').on('dblclick',function() {
			event.preventDefault();
		});*/
	//canvas.addEventListener('mousemove', throttle(onMouseMove, 10), false);

	for (var i = 0; i < colors.length; i++) {
		colors[i].addEventListener('click', onColorUpdate, false);
	}

	$('#palette').on("mousemove mousedown touchstart touchmove", ".palette_item", function(e) {
		e.preventDefault();
		if (e.type == "mousedown"){
			if (cntrlIsPressed == true) {
				//current.fill = e.target.className.split(' ')[1];
				current.fill = e.target.getAttribute("data-rgb");
				console.log('fill:' + current.fill + ' ' + typeof(current.fill));
			} else {
				//current.color = e.target.className.split(' ')[1];
				current.color = e.target.getAttribute("data-rgb");
				console.log('fill:' + current.fill + ' ' + typeof(current.fill));
			}
		};
	});

/*	function onColorUpdate(e) {
		if (cntrlIsPressed == true) {
			current.fill = e.target.className.split(' ')[1];
			console.log('fill:' + current.fill + ' ' + typeof(current.fill));
		} else {
			current.color = e.target.className.split(' ')[1];
			console.log('fill:' + current.fill + ' ' + typeof(current.fill));
		}
	}*/


	socket.on('onPenMove', onPenMove);
	socket.on('onRectMove', onRectMove);
	socket.on('onInitRect', onInitRect);
	socket.on('onFinishRect', onFinishRect);
	socket.on('onInitPen', onInitPen);
	socket.on('onFinishPen', onFinishPen);
	socket.on('onLineMove', onLineMove);
	socket.on('onLineInit', onLineInit);
	socket.on('onClientAdd', onClientAdd);
	socket.on('onClientRemove', onClientRemove);
	socket.on('onMyId', onMyId);
	socket.on('onInitPolygon', onInitPolygon);
	socket.on('onPropPolygon', onPropPolygon);
	socket.on('onFinishPolygon', onFinishPolygon);
	socket.on('onPolygonMove', onPolygonMove);

	function onClientAdd(data) {
		info[data] = { ir: 0, il: 0, iline: 0, ipolygon: 0, points: '' };
		//console.log('info json: '+JSON.stringify(info));
		////console.log('  client add: '+data);
	}

	function onClientRemove(data) {
		delete info[data];
		//console.log('info json: '+JSON.stringify(info));
		////console.log('delet: '+data);
	}

	function onMyId(data) {
		myId = data.myId;
		for (i = 0; i < data.clients.length; i++) {
			info[data.clients[i]] = { ir: 0, il: 0, iline: 0, ipolygon: 0, points: '' };
			//console.log('info json: '+JSON.stringify(info));
			// //console.log(info)
		};
		////console.log("type: "+data.clients[0]);
		////console.log('myId : '+ myId);
	}

	// window.addEventListener('resize', onResize, false);
	// onResize();

	function onMouseDown(e) {
		drawing = true;
		current.x = e.clientX - parseInt($('#mysvg').offset().left);
		current.y = e.clientY - parseInt($('#mysvg').offset().top);
		initial.x = current.x;
		initial.y = current.y;

		switch (current.mode) {
			case 'pen':
				////console.log('info[myId]:'+info[myId]);
				////console.log('mousedown 1 info json: '+JSON.stringify(info));
				////console.log(info[myId]['il']);
				//console.log('info[myId][il]: '+info[myId]['il']);
				info[myId]['il'] = context.path('M ' + current.x + ' ' + current.y + ' ' + ' ').fill(current.fill);
				info[myId]['il'].stroke({ color: current.color, width: current.strokeWidth, linecap: 'round', linejoin: 'round' });
				//console.log('info[myId][il] : '+info[myId]['il']);

				////console.log(typeof(info));
				////console.log(info[myId]['il']);
				////console.log('mousedown 2 info json: '+JSON.stringify(info));
				//console.log('mouseDown me: '+'info[myId]il.d  '+info[myId]['il'].attr('d')+'   '+'myId:'+myId+'  info[myId'+info[myId]);
				socket.emit('onInitPen', { x: current.x, y: current.y, color: current.color, strokeWidth: current.strokeWidth, clientId: myId, fill: current.fill });


			case 'rect':
				info[myId]['ir'] = context.rect(0, 0).attr({ fill: current.fill, stroke: current.color, 'stroke-width': current.strokeWidth }).move(initial.x, initial.y);
				socket.emit('onInitRect', { ix: initial.x, iy: initial.y, fill: current.fill, stroke: current.color, strokeWidth: current.strokeWidth, clientId: myId });
				break;
			case 'line':
				info[myId]['iline'] = context.line(current.x, current.y, current.x, current.y);
				info[myId]['iline'].stroke({ color: current.color, width: current.strokeWidth, linecap: 'round' });
				socket.emit('onLineInit', { x: current.x, y: current.y, color: current.color, strokeWidth: current.strokeWidth, clientId: myId });
				break;
			case 'polygon':
				if (event.which == 2 || event.which == 3) {
					info[myId]['points'] = '';
					info[myId]['ipolygon'] = 0;
					drawing = false;
					socket.emit('onFinishPolygon', { clientId: myId });

					break;
				}
				if (info[myId]['ipolygon'] == 0) {
					info[myId]['ipolygon'] = context.polygon(current.x + ',' + current.y).attr({ fill: current.fill, stroke: current.color, 'stroke-width': current.strokeWidth }).on('dblclick', function(){
						this.selectize({deepSelect:true});
					});
					//.dblclick(function(){console.log('goog');});
					socket.emit('onInitPolygon', { clientId: myId, x: current.x / canvasWidth, y: current.y / canvasHeight, stroke: current.color, strokeWidth: current.strokeWidth, fill: current.fill });
				}
				info[myId]['points'] = info[myId]['points'] + ' ' + current.x + ',' + current.y + ' ';
				socket.emit('onPropPolygon', { clientId: myId, x: current.x / canvasWidth, y: current.y / canvasHeight });
				break;



		}
	}

	function onMouseUp(e) {
		if (!drawing) {
			return;
		}
		drawing = false;
		/////////////////////////////////

		switch (current.mode) {
			case 'pen':
				info[myId]['il'] = 0;
				socket.emit('onFinishPen', { clientId: myId });
				break;
			case 'rect':
				info[myId]['ir'] = 0; // ir = initial rectangle.
				socket.emit('onFinishRect', { clientId: myId });
				break;
			case 'polygon':
				drawing = true;
				break;
		}
	}

	function onMouseMove(e) {
		////console.log('onMouseMove drawing:'+drawing);
		if (!drawing) {
			return;
		}
		current.x = e.clientX - parseInt($('#mysvg').offset().left);
		current.y = e.clientY - parseInt($('#mysvg').offset().top);
		switch (current.mode) {
			case 'pen':
				var w = parseInt(canvas.style.width);
				var h = parseInt(canvas.style.height);
				drawPen(current.x, current.y, current.color); //true means emit is true.
				socket.emit('onPenMove', { x: current.x / w, y: current.y / h, color: current.color, clientId: myId });
				break;
			case 'rect':
				drawRect(initial.x, initial.y, current.x - initial.x, current.y - initial.y, 'none', true);
				break;
			case 'line':
				drawLine(current.x, current.y, 'none', true);
				break;
			case 'polygon':
				drawPolygon(current.x, current.y, 'none', true);
				break;
		}
	}
	//================================================================================================================


	function drawPen(x, y, color) {
		info[myId]['il'].attr('d', info[myId]['il'].attr('d') + ' L ' + x + ' ' + y + ' ');
		////console.log('mousemove me: '+info[myId]['il'].attr('d')+ '    ' + ' clientId,myId:  '+clientId+'  '+myId );
		//console.log('mousemove me: '+info[myId]['il'].attr('d')+ '    ' + ' clientId,myId:  '+'clientId'+'  '+myId );
		/*		info[clientId]['il'].attr('d',info[clientId]['il'].attr('d')+' L '+x+' '+y+' ');
				////console.log('mousemove client: '+info[clientId]['il'].attr('d') + '    ' + ' clientId,myId:  '+clientId+'  '+myId );
				//console.log('mousemove client : info myId '+info[myId]['il'].attr('d')+ '    ' + 'info clientId: '+ info[clientId]['il'].attr('d') );*/

	}

	function drawRect(ix, iy, w, h, clientId, emit) {
		if (emit == true) {
			if (w < 0 && h < 0) {
				info[myId]['ir'].attr({ x: ix + w, y: iy + h, width: -w, height: -h });
			} else if (w < 0) {
				info[myId]['ir'].attr({ x: ix + w, y: iy, width: -w, height: h });
			} else if (h < 0) {
				info[myId]['ir'].attr({ x: ix, y: iy + h, width: w, height: -h });
			} else {
				info[myId]['ir'].attr({ x: ix, y: iy, width: w, height: h });
			}
		} else {
			if (w < 0 && h < 0) {
				info[clientId]['ir'].attr({ x: ix + w, y: iy + h, width: -w, height: -h });
			} else if (w < 0) {
				info[clientId]['ir'].attr({ x: ix + w, y: iy, width: -w, height: h });
			} else if (h < 0) {
				info[clientId]['ir'].attr({ x: ix, y: iy + h, width: w, height: -h });
			} else {
				info[clientId]['ir'].attr({ x: ix, y: iy, width: w, height: h });
			}
		};


		if (!emit) {
			return;
		}
		var wd = parseInt(canvas.style.width);
		var hg = parseInt(canvas.style.height);

		socket.emit('onRectMove', {
			ix: initial.x / wd,
			iy: initial.y / hg,
			w: w,
			h: h,
			clientId: myId
		});

	}

	function drawLine(x, y, clientId, emit) {
		if (emit == true) {
			info[myId]['iline'].attr({ x2: x, y2: y });
		} else {
			info[clientId]['iline'].attr({ x2: x, y2: y });
		};


		if (!emit) {
			return;
		}

		var wd = parseInt(canvas.style.width);
		var hg = parseInt(canvas.style.height);
		socket.emit('onLineMove', {
			x: x / wd,
			y: y / hg,
			clientId: myId
		});
	}

	function drawPolygon(x, y, clientId, emit) {
		if (emit == true) {
			info[myId]['ipolygon'].attr('points', info[myId]['points'] + ' ' + x + ',' + y);
		} else {
			info[clientId]['ipolygon'].attr('points', info[clientId]['points'] + ' ' + x + ',' + y);
		}

		if (!emit) {
			return;
		}

		var wd = parseInt(canvas.style.width);
		var hg = parseInt(canvas.style.height);
		socket.emit('onPolygonMove', {
			x: x / wd,
			y: y / hg,
			clientId: myId
		});

	}

	//==============================================================================================================

	function onColorUpdate(e) {
		if (cntrlIsPressed == true) {
			current.fill = e.target.className.split(' ')[1];
			console.log('fill:' + current.fill + ' ' + typeof(current.fill));
		} else {
			current.color = e.target.className.split(' ')[1];
			console.log('fill:' + current.fill + ' ' + typeof(current.fill));
		}
	}

	// limit the number of events per second
	function throttle(callback, delay) {
		var previousCall = new Date().getTime();
		return function() {
			var time = new Date().getTime();

			if ((time - previousCall) >= delay) {
				previousCall = time;
				callback.apply(null, arguments);
			}
		};
	}
	///////////////////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//for pen
	function onInitPen(data) {
		//console.log('myId '+myId+' data.clientId : '+data.clientId);
		//console.log('  info[myId]: '+info[myId]['il']);
		//console.log('  info[data.clientId]: '+info[data.clientId]['il']);
		info[data.clientId]['il'] = context.path('M ' + data.x + ' ' + data.y + ' ').fill(data.fill);
		info[data.clientId]['il'].stroke({ color: data.color, width: data.strokeWidth, linecap: 'round', linejoin: 'round' });
		//console.log('  info[myId][il]: '+info[myId]['il']);
		//console.log('  info[data.clientId][il]: '+info[data.clientId]['il']);
		//console.log('mouseDown client : info myId '+info[myId]['il']+ '    ' + 'info data.clientId: '+ info[data.clientId]['il'].attr('d') + ' data.clientId '+ data.clientId +  ' myId '+ myId);
		////console.log('onInitPen:'+info[clientId]['il']);
	}

	function onFinishPen(data) {
		info[data.clientId]['il'] = 0;
	}


	function onPenMove(data) {
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);
		//////console.log('client: ' + w);
		info[data.clientId]['il'].attr('d', info[data.clientId]['il'].attr('d') + ' L ' + data.x * w + ' ' + data.y * h + ' ');
		/*		drawPen(data.x * w, data.y * h, data.color, data.clientId, false);*/
	}
	//for rectangle
	function onRectMove(data) {
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);
		////console.log('client received emition. ');
		drawRect(data.ix * w, data.iy * h, data.w, data.h, data.clientId, false);
		////console.log('drawrect is called');
	}

	function onInitRect(data) {
		info[data.clientId]['ir'] = context.rect(0, 0).attr({ x: data.ix, y: data.iy, fill: data.fill, stroke: data.stroke, 'stroke-width': data.strokeWidth });
		////console.log('rect is initiated x:'+ ir.attr('x'));
	};

	function onFinishRect(data) {
		info[data.clientId]['ir'] = 0;
	};

	function onLineInit(data) {
		info[data.clientId]['iline'] = context.line(data.x, data.y, data.x, data.y);
		info[data.clientId]['iline'].stroke({ color: data.color, width: data.strokeWidth, linecap: 'round' });
	}

	function onLineMove(data) {
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);

		drawLine(data.x * w, data.y * h, data.clientId, false);
	}

	// for polygon

	function onInitPolygon(data) {
		info[data.clientId]['ipolygon'] = context.polygon(data.x * canvasWidth + ',' + data.y * canvasHeight).attr({ fill: data.fill, stroke: data.stroke, 'stroke-width': data.strokeWidth });
	}

	function onPropPolygon(data) {
		info[data.clientId]['points'] = info[data.clientId]['points'] + ' ' + data.x * canvasWidth + ',' + data.y * canvasHeight + ' ';
	}

	function onFinishPolygon(data) {
		info[data.clientId]['points'] = '';
		info[data.clientId]['ipolygon'] = 0;
	}

	function onPolygonMove(data) {
		drawPolygon(data.x * canvasWidth, data.y * canvasHeight, data.clientId, false);
	}


	//for resizing
	// make the canvas fill its parent
	/*	 function onResize() {
			canvas.style.width = window.innerWidth;
			canvas.style.height = window.innerHeight;
		} */

})();
