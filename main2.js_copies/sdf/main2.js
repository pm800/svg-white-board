(function() {
	var ir = 0,  iline=0, ipolygon=0;
	var ir_2 = 0,  iline_2=0, ipolygon_2=0;
	var points = '';
	var points_2 = '';
	var info = {};
	console.log('info json: '+JSON.stringify(info));
	var ilist = {ir:0, il:0, iline:0, ipolygon:0};
	console.log('mousedown json: '+JSON.stringify(ilist));
	var myId = '';

	var socket = io();
	var canvas = document.getElementById('mysvg');
	//console.log(canvas);
	var colors = document.getElementsByClassName('color');
	//var context = Snap('#svg');
	var context = SVG('mysvg');


	$(document).ready(function(){
		$('#pen').click(function(){
			current.mode = 'pen';
		});
		$('#rect').click(function(){
			current.mode = 'rect';
		});
		$('#line').click(function(){
			current.mode = 'line';
		});
		$('#polygon').click(function(){
			current.mode = 'polygon';
		});
	});

	var current = {
		color: 'black',
		mode : 'pen'
	};
	var initial = {
		x: '0'
	};
	var drawing = false;

	canvas.addEventListener('mousedown', onMouseDown, false);
	canvas.addEventListener('mouseup', onMouseUp, false);

	//canvas.addEventListener('mouseout', onMouseUp, false);
	//$('svg').mouseleave(onMouseUp);
	$('svg').on('mousemove', throttle(onMouseMove, 10));
/*	$('svg').on('dblclick',function() {
		event.preventDefault();
	});*/
	//canvas.addEventListener('mousemove', throttle(onMouseMove, 10), false);

	for (var i = 0; i < colors.length; i++){
		colors[i].addEventListener('click', onColorUpdate, false);
	}

	socket.on('onPenMove', onPenMove);
	socket.on('onRectMove', onRectMove);
	socket.on ('onInitRect', onInitRect);
	socket.on('onFinishRect', onFinishRect);
	socket.on('onInitPen', onInitPen);
	socket.on('onFinishPen', onFinishPen);
	socket.on('onLineMove', onLineMove);
	socket.on('onLineInit', onLineInit);
	socket.on('onClientAdd', onClientAdd );
	socket.on('onClientRemove', onClientRemove);
	socket.on('onMyId', onMyId);

	function onClientAdd(data){
		info[data]=ilist;
		console.log('info json: '+JSON.stringify(info));
		//console.log('  client add: '+data);
	}
	function onClientRemove(data){
		delete info[data];
		console.log('info json: '+JSON.stringify(info));
		//console.log('delet: '+data);
	}
	function onMyId(data){
		myId = data.myId;
		for(i=0; i<data.clients.length; i++){
			info[data.clients[i]] = {ir:0, il:0, iline:0, ipolygon:0};
			console.log('info json: '+JSON.stringify(info));
			// console.log(info)
		};
		//console.log("type: "+data.clients[0]);
		//console.log('myId : '+ myId);
	}

	// window.addEventListener('resize', onResize, false);
	// onResize();





	function drawPen(x, y, color, clientId, emit){
		if (emit==true){
			info[myId]['il'].attr('d',info[myId]['il'].attr('d')+' L '+x+' '+y+' ');
			//console.log('mousemove me: '+info[myId]['il'].attr('d')+ '    ' + ' clientId,myId:  '+clientId+'  '+myId );
			console.log('mousemove me: '+info[myId]['il'].attr('d')+ '    ' + ' clientId,myId:  '+clientId+'  '+myId );
		} else {
			info[clientId]['il'].attr('d',info[clientId]['il'].attr('d')+' L '+x+' '+y+' ');
			//console.log('mousemove client: '+info[clientId]['il'].attr('d') + '    ' + ' clientId,myId:  '+clientId+'  '+myId );
			console.log('mousemove client : info myId '+info[myId]['il'].attr('d')+ '    ' + 'info clientId: '+ info[clientId]['il'].attr('d') );

		};
		if (!emit) { return; }
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);
		console.log('emmiting');
		socket.emit('onPenMove', {
			x: x / w,
			y: y / h,
			color: color,
			clientId:myId
		});
	}


	function onInitPen(data){
		info[data.clientId]['il'] = context.path('M '+data.x + ' '+data.y+' ').fill('none');
		info[data.clientId]['il'].stroke({ color: data.color, width: data.strokeWidth, linecap: 'round', linejoin: 'round' });
		console.log('mouseDown client: '+info[data.clientId]['il'].attr('d')  );
		//console.log('onInitPen:'+info[clientId]['il']);
	}
	
	function onFinishPen(data){
		info[data.clientId]['il'] = 0;
	}

	function onPenMove(data){
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);
		////console.log('client: ' + w);
		drawPen(data.x * w, data.y * h, data.color, data.clientId, false);
	}

	function onMouseDown(e){
		drawing = true;
		current.x = e.clientX-parseInt($('#mysvg').offset().left);
		current.y = e.clientY-parseInt($('#mysvg').offset().top);
		initial.x = current.x;
		initial.y = current.y;

		switch (current.mode) {
			case 'pen' :
				//console.log('info[myId]:'+info[myId]);
				//console.log('mousedown 1 info json: '+JSON.stringify(info));
				//console.log(info[myId]['il']);
				info[myId]['il'] = context.path('M '+current.x + ' '+current.y+' ').fill('none');
				info[myId]['il'].stroke({ color: current.color, width: 3, linecap: 'round', linejoin: 'round' });
				//console.log(typeof(info));
				//console.log(info[myId]['il']);
				//console.log('mousedown 2 info json: '+JSON.stringify(info));
				console.log('mouseDown me: '+info[myId]['il'].attr('d')+'   '+'myId:'+myId);
				socket.emit('onInitPen', {x:current.x, y:current.y, color:current.color, strokeWidth: '3px', clientId:myId});

			case 'rect' :
				ir = context.rect(0,0).attr({fill:"none",stroke:current.color, 'stroke-width':"3px"}).move(initial.x, initial.y);
				socket.emit('onInitRect',{ix:initial.x, iy:initial.y, fill:'none', stroke:current.color, strokeWidth:'3px', clientId:myId});
				break;
			case 'line':
				iline = context.line(current.x, current.y, current.x, current.y);
				iline.stroke({ color: current.color, width: 3, linecap: 'round' });
				socket.emit('onLineInit',{x:current.x, y:current.y, color:current.color, clientId:myId});
				break;
			case 'polygon':
				if (event.which == 2 || event.which==3){
					event.preventDefault();
					points = '';
					ipolygon = 0;
					drawing = false;
					break;
				}
				if (ipolygon == 0){
					ipolygon = context.polygon(current.x+','+current.y).attr({fill:"none",stroke:current.color, 'stroke-width':"3px"});
				}
				points = points + ' '+ current.x+','+current.y+' ';
				break;

				

		}
	}

	function onMouseUp(e){
		if (!drawing) { return; }
		drawing = false;
		/////////////////////////////////

		switch (current.mode) {
			case 'pen' :
				info[myId]['il'] = 0;
				socket.emit('onFinishPen', { clientId:myId});
				break;
			case 'rect' :
				ir = 0; // ir = initial rectangle.
				socket.emit('onFinishRect',{ clientId:myId});
				break;
			case 'polygon':
				drawing = true;
				break;
		}
	}

	function onMouseMove(e){
		//console.log('onMouseMove drawing:'+drawing);
		if (!drawing) { return; }
		current.x = e.clientX-parseInt($('#mysvg').offset().left);
		current.y = e.clientY-parseInt($('#mysvg').offset().top);
		switch (current.mode) {
			case 'pen' :
				drawPen(current.x, current.y, current.color, 'someid',  true); //true means emit is true.
				break;
			case 'rect' :
				drawRect(initial.x, initial.y, current.x - initial.x, current.y - initial.y, 'none', true);
				break;
			case 'line' :
				drawLine(current.x, current.y,  'none', true);
				break;
			case 'polygon':
				drawPolygon(current.x, current.y,  'none', true);
				break;
		}
	}


	function drawRect(ix,iy,w,h, clientId, emit){
		if (emit==true){
			if (w<0 && h<0){
				ir.attr({x:ix+w, y:iy+h, width: -w, height: -h});
			} else if (w<0) {
				ir.attr({x:ix+w, y:iy, width: -w, height:h});
			} else if (h<0){
				ir.attr({x:ix, y:iy+h, width: w, height: -h});
			} else {
				ir.attr({x:ix, y:iy, width: w, height:h});
			}	
		} else {
			if (w<0 && h<0){
				ir_2.attr({x:ix+w, y:iy+h, width: -w, height: -h});
			} else if (w<0) {
				ir_2.attr({x:ix+w, y:iy, width: -w, height:h});
			} else if (h<0){
				ir_2.attr({x:ix, y:iy+h, width: w, height: -h});
			} else {
				ir_2.attr({x:ix, y:iy, width: w, height:h});
			}	
		};

		
		if (!emit) { return; }
		var wd = parseInt(canvas.style.width);
		var hg = parseInt(canvas.style.height);

		socket.emit('onRectMove', {
			ix:initial.x / wd,
			iy : initial.y / hg,
			w : w,
			h : h,
			clientId:myId
		});
		
	}

	function drawLine(x,y, clientId, emit){
		if(emit==true){
			iline.attr({x2:x, y2:y});
		} else {
			iline_2.attr({x2:x, y2:y});
		};
		

		if (!emit) { return; }

		var wd = parseInt(canvas.style.width);
		var hg = parseInt(canvas.style.height);
		socket.emit('onLineMove', {
			x : x / wd,
			y : y / hg,
			clientId:myId
		});
	}

	function drawPolygon(x,y, clientId, emit){
		if (emit==true){
			ipolygon.attr({points: points+' '+x+','+y});
		} else {
			ipolygon_2.attr({points: points+' '+x+','+y});
		}
	}
	function onColorUpdate(e){
		current.color = e.target.className.split(' ')[1];
	}

	// limit the number of events per second
	function throttle(callback, delay) {
		var previousCall = new Date().getTime();
		return function() {
			var time = new Date().getTime();

			if ((time - previousCall) >= delay) {
				previousCall = time;
				callback.apply(null, arguments);
			}
		};
	} 
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
//for pen

//for rectangle
	function onRectMove(data){
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);
		//console.log('client received emition. ');
		drawRect(data.ix * w , data.iy * h, data.w ,  data.h, data.clientId, false);
		//console.log('drawrect is called');
	}

	function onInitRect(data){
		ir_2 =  context.rect(0, 0).attr({x:data.ix, y:data.iy, fill:data.fill, stroke: data.stroke, 'stroke-width':data.strokeWidth});
		//console.log('rect is initiated x:'+ ir.attr('x'));
	};

	function onFinishRect(data){
		ir_2 = 0;
	};

	function onLineInit(data){
		iline_2 = context.line(data.x, data.y, data.x, data.y);
		iline_2.stroke({ color: data.color, width: 3, linecap: 'round' });
	}

	function onLineMove(data){
		var w = parseInt(canvas.style.width);
		var h = parseInt(canvas.style.height);

		drawLine(data.x * w, data.y * h, data.clientId, false);
	}

//for resizing
	// make the canvas fill its parent
/*	 function onResize() {
		canvas.style.width = window.innerWidth;
		canvas.style.height = window.innerHeight;
	} */

})();
